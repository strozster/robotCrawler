/*
 * StateMachine.c
 *
 *  Created on: Oct 23, 2017
 *      Author: Andrew Stroz
 */

#include "StateMachine.h"
#include "stdio.h"
#include "pca9685.h"
#include "cfg.h"
#include "LegStateMachine.h"

static void(*CurrentState)(bool changeOfState);
static void(*PreviousState)(bool changeOfState);

static void Init(bool changeOfState);
static void Sequencer(bool changeOfState);

static void Rest(bool changeOfState);

static void Walk_1(bool changeOfState);
static void Walk_2(bool changeOfState);
static void Walk_3(bool changeOfState);
static void Walk_4(bool changeOfState);
static void Walk_5(bool changeOfState);
static void Walk_6(bool changeOfState);
static void Walk_7(bool changeOfState);
static void Walk_8(bool changeOfState);
static void Walk_9(bool changeOfState);
static void Walk_10(bool changeOfState);

static void Rot_1(bool changeOfState);
static void Rot_2(bool changeOfState);
static void Rot_3(bool changeOfState);
static void Rot_4(bool changeOfState);
static void Rot_5(bool changeOfState);
static void Rot_6(bool changeOfState);
static void Rot_7(bool changeOfState);
static void Rot_8(bool changeOfState);

static uint16_t stateTimer;
static uint16_t statePeriod;

static uint8_t reverse = 0, switch_rev = 0;
static uint8_t rotate = 0;
static leg_t leg;
static uint8_t temp = 0;

static sequencer_t last_sequence, current_sequence;
/*
    Initialize the state machine.

*/
void StateMachineInit()
{
    PreviousState = NULL;
    CurrentState = Init;
    stateTimer = 0;

}

/*
    Called from the task scheduler.

    taskPeriod: Time elapsed since last call (in milliseconds).
*/
void StateMachineTask(uint16_t taskPeriod)
{
    bool changeOfState;
    static uint16_t startDelay = 0;

    if (startDelay < STATE_START_DELAY) {
        startDelay += taskPeriod;
        return;
    }

    // A free running counter.
    stateTimer += taskPeriod;
    statePeriod = taskPeriod;

    // Flag a state change.
    if (CurrentState == PreviousState)
        changeOfState = false;
    else
        changeOfState = true;

    PreviousState = CurrentState;

    // Execute state.
    CurrentState(changeOfState);
}

void Init(bool changeOfState)
{
	//TODO:: maybe make a flag thats raised when cfg.c is init allowing the sequencer to proceed...
	leg = DIR_LEG_DEFAULT;
    startLegStateMachine(&leg);

    CurrentState = Sequencer;
}

void Sequencer(bool changeOfState){
	if (changeOfState) {
		// currently not using leg.dir... maybe it could have a use?
		//if (leg.dir == LEFT)
		//	rotate = SERVO_MAX;
		//else
		//	rotate = SERVO_MIN;

		//current_sequence = WALK;
		//leg.skew = 30;
		//leg.style = ALG1;
		rotate = (rand() % SERVO_MAX);

		if (current_sequence == WALK)
			temp++;

		if (current_sequence == WALK && temp > 3){
			last_sequence = current_sequence;
			current_sequence = (rand() % (sizeof(sequencer_t) + 1));
			temp = 0;
		}
		else{
			last_sequence = current_sequence;
			current_sequence = (rand() % (sizeof(sequencer_t) + 1));
		}
	}

	// use last_sequence to reset to REST if previous state was not itself... i.e. going from walking to turning...
	if (last_sequence == current_sequence){
		switch(current_sequence) {
			case WALK:
				CurrentState = Walk_1;
				break;
			case ROTATE:
				CurrentState = Rot_1;
				break;

			default: // REST
				CurrentState = Rest;
				break;
		}
	}
	else{
		CurrentState = Rest;
		leg.style = (rand() % (sizeof(style_t) + 1));
	}
}


// The resting state of the robot with panning s held at xx degrees and tilting
// s held at xx degrees in order for the robot to stand
void Rest(bool changeOfState)
{
    if (changeOfState) {
    	setTopAngle(TOP_LEG_DEFAULT);
    	setBottomAngle(BOT_LEG_DEFAULT);
    }
    CurrentState = Sequencer;
}

// The first walking state, two legs on the right side pointing straight out and two legs
// on the left side pointed diagonally outwards
void Walk_1(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_right,TOP_LEG_FORWARD,BOT_LEG_UP);
		}
		CurrentState = Walk_2;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_right,TOP_LEG_BACKWARD,BOT_LEG_DEFAULT);
			setLegAngle(&leg, &leg.back_left,TOP_LEG_FORWARD,BOT_LEG_DEFAULT);
			setLegAngle(&leg, &leg.front_right,TOP_LEG_BACKWARD,BOT_LEG_DEFAULT);
			setLegAngle(&leg, &leg.front_left,TOP_LEG_FORWARD,BOT_LEG_DEFAULT);

		}
		CurrentState = Walk_2;
	}
}

void Walk_2(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_right,TOP_LEG_FORWARD,BOT_LEG_DEFAULT);
		}
		CurrentState = Walk_3;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_right,TOP_LEG_DEFAULT,BOT_LEG_UP);
		}
		CurrentState = Walk_3;
	}
}

// The second walking state, top right legs reaches out diagonally forward
void Walk_3(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_right,TOP_LEG_DEFAULT,BOT_LEG_UP);
		}
		CurrentState = Walk_5;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_right,TOP_LEG_FORWARD,BOT_LEG_DEFAULT);
		}
		CurrentState = Walk_4;
	}
}


void Walk_4(bool changeOfState)
{
	if (leg.style == ALG1){

	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_right,TOP_LEG_DEFAULT,BOT_LEG_UP);
		}
		CurrentState = Walk_5;
	}
}


// First leg shifting state, right side shifts clockwise and left side shifts
// counter-clockwise to "hump" the robot forward
void Walk_5(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_left,TOP_LEG_FORWARD,BOT_LEG_DEFAULT);
			setLegAngle(&leg, &leg.back_left,TOP_LEG_DEFAULT,BOT_LEG_UP);
			setLegAngle(&leg, &leg.front_right,TOP_LEG_DEFAULT,BOT_LEG_DOWN);
			setLegAngle(&leg, &leg.back_right,TOP_LEG_DEFAULT,BOT_LEG_DEFAULT);
		}
		CurrentState = Walk_7;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_right,TOP_LEG_FORWARD,BOT_LEG_DEFAULT);
		}
		CurrentState = Walk_6;
	}
}

// Mirror of state One
void Walk_6(bool changeOfState)
{
	if (leg.style == ALG1){

	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_left,TOP_LEG_DEFAULT,BOT_LEG_UP);
		}
		CurrentState = Walk_7;
	}
}

// Mirror of state Two
void Walk_7(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_left,TOP_LEG_BACKWARD,BOT_LEG_UP);
		}
		CurrentState = Walk_8;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_left,TOP_LEG_BACKWARD,BOT_LEG_DEFAULT);
		}
		CurrentState = Walk_8;
	}
}

void Walk_8(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.back_left,TOP_LEG_BACKWARD,BOT_LEG_DEFAULT);
		}
		CurrentState = Walk_9;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_left,TOP_LEG_DEFAULT,BOT_LEG_UP);
		}
		CurrentState = Walk_9;
	}
}

void Walk_9(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_left,TOP_LEG_DEFAULT,BOT_LEG_UP);
		}
		CurrentState = Walk_10;
	}
	else{
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_left,TOP_LEG_BACKWARD,BOT_LEG_DEFAULT);
		}
		CurrentState = Sequencer;
	}
}

// Mirror of state Three
void Walk_10(bool changeOfState)
{
	if (leg.style == ALG1){
		if (changeOfState) {
			setLegAngle(&leg, &leg.front_right,TOP_LEG_BACKWARD,BOT_LEG_DEFAULT);
			setLegAngle(&leg, &leg.back_right,TOP_LEG_DEFAULT,BOT_LEG_UP);
			setLegAngle(&leg, &leg.front_left,TOP_LEG_DEFAULT,BOT_LEG_DOWN);
			setLegAngle(&leg, &leg.back_left,TOP_LEG_DEFAULT,BOT_LEG_DEFAULT);
		}

		CurrentState = Sequencer;
	}
	else{

	}

}

// ROTATE in any direction with any angle...
void Rot_1(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.back_right,TOP_LEG_DEFAULT,BOT_LEG_UP);
    CurrentState = Rot_2;
}

void Rot_2(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.back_right,rotate,BOT_LEG_DEFAULT);
    CurrentState = Rot_3;
}

void Rot_3(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.front_right,TOP_LEG_DEFAULT,BOT_LEG_UP);
    CurrentState = Rot_4;
}

void Rot_4(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.front_right,rotate,BOT_LEG_DEFAULT);
    CurrentState = Rot_5;
}

void Rot_5(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.front_left,TOP_LEG_DEFAULT,BOT_LEG_UP);
    CurrentState = Rot_6;
}

void Rot_6(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.front_left,rotate,BOT_LEG_DEFAULT);
    CurrentState = Rot_7;
}

void Rot_7(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.back_left,TOP_LEG_DEFAULT,BOT_LEG_UP);
    CurrentState = Rot_8;
}

void Rot_8(bool changeOfState){
	if (changeOfState)
		setLegAngle(&leg, &leg.back_left,rotate,BOT_LEG_DEFAULT);
    CurrentState = Rest;
}

