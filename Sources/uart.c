/*
 * uart.c
 *
 *  Created on: Oct 1, 2017
 *      Author: Andrew
 */

#include "uart.h"
#include "lpuart1.h"

void initUART(void){
	LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);
}

int _write(int file, char *ptr, int len)
{
	uint32_t bytesRemaining;
	LPUART_DRV_SendData(INST_LPUART1,(uint8_t *)ptr, len);
	while(LPUART_DRV_GetTransmitStatus(INST_LPUART1, &bytesRemaining) != STATUS_SUCCESS);
    return len;
}
