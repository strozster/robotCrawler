/*
 * tasks.c
 *
 *  Created on: Oct 23, 2017
 *      Author: Andrew Stroz
 */

#include "tasks.h"
#include "StateMachine.h"
#include "uart.h"
#include "stdio.h"
#include "LegStateMachine.h"

static void Task_ExecuteAlways();
static void Task_1ms();
static void Task_5ms();
static void Task_10ms();
static void Task_25ms();
static void Task_50ms();
static void Task_100ms();
static void Task_250ms();
static void Task_500ms();
static void Task_1000ms();
static void Task_2000ms();

static StartUp startup = { 0, 1000, false };

uint8_t tick = 0;

/*
    The main task scheduler.
*/
void TaskScheduler()
{
    uint16_t i;

    // List of task time slots.
    static Tasks tasks[] = {
        { TASK_1MS, 0, Task_1ms },
        { TASK_5MS, 0, Task_5ms },
        { TASK_10MS, 0, Task_10ms },
        { TASK_25MS, 0, Task_25ms },
        { TASK_50MS, 0, Task_50ms },
        { TASK_100MS, 0, Task_100ms },
        { TASK_250MS, 0, Task_250ms },
        { TASK_500MS, 0, Task_500ms },
        { TASK_1000MS, 0, Task_1000ms },
        { TASK_2000MS, 0, Task_2000ms }
    };

    const uint8_t numberOfTasks = sizeof(tasks) / sizeof(tasks[0]);

    Task_ExecuteAlways();

    if (tick == 1) {
        tick = 0;

        if (startup.isComplete == false) {
            startup.counter++;
            if (startup.counter >= startup.duration){
                startup.isComplete = true;
			}else {
                return;
			}
        }

        for (i = 0; i < numberOfTasks; i++) {
            tasks[i].counter++;

            if (tasks[i].counter >= tasks[i].resetCount) {
                tasks[i].taskFunction();
                tasks[i].counter = 0;
            }
        }
    }
}

void Task_ExecuteAlways()
{

}

void Task_1ms()
{
    //const uint16_t msPeriod = 1;
	//printf("Hello Andrei :) \r\n");
}

void Task_5ms()
{
    //const uint16_t msPeriod = 5;
}

void Task_10ms()
{
    //const uint16_t msPeriod = 10;
}

void Task_25ms()
{
    //const uint16_t msPeriod = 25;
    //StateMachineTask(msPeriod);
}

void Task_50ms()
{
    //const uint16_t msPeriod = 50;
}

void Task_100ms()
{
    //const uint16_t msPeriod = 100;
}

void Task_250ms()
{
    const uint16_t msPeriod = 250;
    StateMachineTask(msPeriod);
    updateServoAngles();
}


void Task_500ms()
{
   // const uint16_t msPeriod = 500;
   // StateMachineTask(msPeriod);
   // updateServoAngles();
}


void Task_1000ms()
{
    //const uint16_t msPeriod = 1000;
	//StateMachineTask(msPeriod);
}

void Task_2000ms()
{
    //const uint16_t msPeriod = 2000;
}


