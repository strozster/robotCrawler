/*
 * pca9685.c
 *
 *  Created on: Oct 22, 2017
 *      Author: Andrew Stroz
 */

#include "pca9685.h"
#include "lpi2c1.h"
#include "i2c.h"
#include <stdio.h>

void autoIncrementI2CWriteOn();
void autoIncrementI2CWriteOff();

static uint8_t servoStateBuffer[33];

void sendInitaliztionDataToPCA9685(){
	uint8_t test = 0;
	i2cWrite8(PCA9685_MODE1, 0b10100000); //reset device
	//i2cWrite8(PCA9685_MODE1, 0b10000000); //reset device

	setPWMFreq(50); // 50 MHZ
	test = i2cRead8(PCA9685_MODE1);

	autoIncrementI2CWriteOn();
}

void setBottomServoAngle(uint8_t degrees){
	uint32_t countsLow = SERVO_TICKS_LENGTH - 1;
	uint32_t countsHigh = servo_tick_bias + (degrees / servo_tick_angular_res);

	uint8_t i = 0;

	servoStateBuffer[0] = 0x06;
	for (i = 0; i < 8; i++){
		if (i%2 == 1){
			servoStateBuffer[(i*4) + 1] = (uint8_t)countsLow;
			servoStateBuffer[(i*4) + 2] = (uint8_t)(countsLow >> 8);
			servoStateBuffer[(i*4) + 3] = (uint8_t)countsHigh;
			servoStateBuffer[(i*4) + 4] = (uint8_t)(countsHigh >> 8);
		}
	}
	//autoIncrementI2CWriteOn();
	i2cWriteBuffer8(servoStateBuffer, 33);
	//autoIncrementI2CWriteOff();
}

void setTopServoAngle(uint8_t degrees){
	uint32_t countsLow = SERVO_TICKS_LENGTH - 1;
	uint32_t countsHigh = servo_tick_bias + (degrees / servo_tick_angular_res);

	uint8_t i = 0;

	servoStateBuffer[0] = 0x06;
	for (i = 0; i < 8; i++){
		if (i%2 == 0){
			servoStateBuffer[(i*4) + 1] = (uint8_t)countsLow;
			servoStateBuffer[(i*4) + 2] = (uint8_t)(countsLow >> 8);
			servoStateBuffer[(i*4) + 3] = (uint8_t)countsHigh;
			servoStateBuffer[(i*4) + 4] = (uint8_t)(countsHigh >> 8);
		}
	}
	//autoIncrementI2CWriteOn();
	i2cWriteBuffer8(servoStateBuffer, 33);
	//autoIncrementI2CWriteOff();
}

void setAllServoAngle(uint8_t degrees){
	uint32_t countsLow = SERVO_TICKS_LENGTH - 1;
	uint32_t countsHigh = servo_tick_bias + (degrees / servo_tick_angular_res);

	uint8_t i = 0;

	servoStateBuffer[0] = 0x06;
	for (i = 0; i < 8; i++){
		servoStateBuffer[(i*4) + 1] = (uint8_t)countsLow;
		servoStateBuffer[(i*4) + 2] = (uint8_t)(countsLow >> 8);
		servoStateBuffer[(i*4) + 3] = (uint8_t)countsHigh;
		servoStateBuffer[(i*4) + 4] = (uint8_t)(countsHigh >> 8);
	}
	//autoIncrementI2CWriteOn();
	i2cWriteBuffer8(servoStateBuffer, 33);
	//autoIncrementI2CWriteOff();
}

void setLegServoAngle(uint8_t channel, uint8_t degreesTop, uint8_t degreesBottom){
	static uint8_t buffer[9];
	uint32_t countsLowTop = SERVO_TICKS_LENGTH - 1;
	uint32_t countsHighTop = servo_tick_bias + (degreesTop / servo_tick_angular_res);
	uint32_t countsLowBottom = SERVO_TICKS_LENGTH - 1;
	uint32_t countsHighBottom = servo_tick_bias + (degreesBottom / servo_tick_angular_res);

	buffer[0] = 0x06 + channel * 4;
	buffer[1] = (uint8_t)countsLowTop; //OFF_L
	buffer[2] = (uint8_t)(countsLowTop >> 8); //OFF_H
	buffer[3] = (uint8_t)countsHighTop; //ON_L
	buffer[4] = (uint8_t)(countsHighTop >> 8); //ON_H
	buffer[5] = (uint8_t)countsLowBottom; //OFF_L
	buffer[6] = (uint8_t)(countsLowBottom >> 8); //OFF_H
	buffer[7] = (uint8_t)countsHighBottom; //ON_L
	buffer[8] = (uint8_t)(countsHighBottom >> 8); //ON_H

	servoStateBuffer[(channel*4) + 1] = (uint8_t)countsLowTop; //OFF_L
	servoStateBuffer[(channel*4) + 2] = (uint8_t)(countsLowTop >> 8); //OFF_H
	servoStateBuffer[(channel*4) + 3] = (uint8_t)countsHighTop; //ON_L
	servoStateBuffer[(channel*4) + 4] = (uint8_t)(countsHighTop >> 8); //ON_H
	servoStateBuffer[(channel*4) + 5] = (uint8_t)countsLowBottom; //OFF_L
	servoStateBuffer[(channel*4) + 6] = (uint8_t)(countsLowBottom >> 8); //OFF_H
	servoStateBuffer[(channel*4) + 7] = (uint8_t)countsHighBottom; //ON_L
	servoStateBuffer[(channel*4) + 8] = (uint8_t)(countsHighBottom >> 8); //ON_H

	//autoIncrementI2CWriteOn();
	i2cWriteBuffer8(buffer, 9);
	//autoIncrementI2CWriteOff();
}

void setServoAngle(uint8_t channel, uint8_t degrees){
	static uint8_t buffer[5];

	uint32_t countsLow = SERVO_TICKS_LENGTH - 1;
	uint32_t countsHigh = servo_tick_bias + (degrees / servo_tick_angular_res);

	buffer[0] = 0x06 + channel * 4;
	buffer[1] = (uint8_t)countsLow; //OFF_L
	buffer[2] = (uint8_t)(countsLow >> 8); //OFF_H
	buffer[3] = (uint8_t)countsHigh; //ON_L
	buffer[4] = (uint8_t)(countsHigh >> 8); //ON_H

	servoStateBuffer[(channel*4) + 1] = (uint8_t)countsLow; //OFF_L
	servoStateBuffer[(channel*4) + 2] = (uint8_t)(countsLow >> 8); //OFF_H
	servoStateBuffer[(channel*4) + 3] = (uint8_t)countsHigh; //ON_L
	servoStateBuffer[(channel*4) + 4] = (uint8_t)(countsHigh >> 8); //ON_H

	//autoIncrementI2CWriteOn();
	i2cWriteBuffer8(buffer, 5);
	//autoIncrementI2CWriteOff();
}

void setPWMFreq(float freq) {
	// servo param as func of freq
	servo_tick_rate = (float)(SERVO_TICKS_LENGTH * freq / 1000); // ticks/ms
	servo_tick_bias = (float)(SERVO_POSITION_180 * servo_tick_rate); // position 0 tick bias
	servo_tick_angular_res = (float)(SERVO_ROTATION_LENGTH / ((SERVO_POSITION_0 - SERVO_POSITION_180) * servo_tick_rate)); // degrees/tick

	uint8_t test = 0;
	freq = freq *0.9;  // Correct for overshoot in the frequency setting (see issue #11). AKA dampening
	float prescaleval = 25000000;
	prescaleval = prescaleval/ 4096;
	prescaleval = prescaleval /freq;
	prescaleval = prescaleval - 1;

	uint8_t prescale = (uint8_t)(prescaleval + 0.5); //should be floored

	uint8_t oldmode = i2cRead8(PCA9685_MODE1);
	uint8_t newmode = (oldmode&0x7F) | 0x10; // sleep
	i2cWrite8(PCA9685_MODE1, newmode); // go to sleep
	i2cWrite8(PCA9685_PRESCALE, prescale); // set the prescaler
	i2cWrite8(PCA9685_MODE1, oldmode);
	//delay(5);
	printf("                                            ");

	test = i2cRead8(PCA9685_MODE1);
	test = i2cRead8(PCA9685_PRESCALE);

	test = i2cRead8(PCA9685_MODE1);
}

void autoIncrementI2CWriteOn(){
	uint8_t oldmode = i2cRead8(PCA9685_MODE1);
	uint8_t newmode = (oldmode&0x7F) | 0b00100000; // auto increment mode on
	i2cWrite8(PCA9685_MODE1, newmode); //go to auto increment mode
}
void autoIncrementI2CWriteOff(){
	uint8_t oldmode = i2cRead8(PCA9685_MODE1);
	uint8_t newmode = (oldmode & ~(0b00100000)); // auto increment mode off
	i2cWrite8(PCA9685_MODE1, newmode); //go out of auto increment mode
}
