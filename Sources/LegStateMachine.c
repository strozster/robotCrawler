/*
 * LegStateMachine.c
 *
 *  Created on: Jan 28, 2018
 *      Author: Andrew Stroz
 */


#include "LegStateMachine.h"
#include "pca9685.h"
#include "stdio.h"
#include "string.h"

static leg_t * currentPosition;
static leg_t futurePosition;
static uint8_t startLegStateMachineFlag = 0;
static legChanges_t legChanges;

void startLegStateMachine(leg_t * currentState){
	currentPosition = currentState;
	legChanges.all = 0;
	legChanges.top = 0;
	legChanges.bottom = 0;
	legChanges.legs = 0;
	startLegStateMachineFlag = 1;

	memcpy(&futurePosition,currentPosition,sizeof(legChanges_t));
}

void setAllAngle(uint8_t degrees){
	futurePosition.back_left.bot_pos = degrees;
	futurePosition.back_left.top_pos = degrees;
	futurePosition.front_left.bot_pos = degrees;
	futurePosition.front_left.top_pos = degrees;
	futurePosition.back_right.bot_pos = degrees;
	futurePosition.back_right.top_pos = degrees;
	futurePosition.front_right.bot_pos = degrees;
	futurePosition.front_right.top_pos = degrees;
	legChanges.all = 1;
}
void setBottomAngle(uint8_t degrees){
	futurePosition.back_left.bot_pos = degrees;
	futurePosition.front_left.bot_pos = degrees;
	futurePosition.back_right.bot_pos = degrees;
	futurePosition.front_right.bot_pos = degrees;
	legChanges.bottom = 1;
}
void setTopAngle(uint8_t degrees){
	futurePosition.back_left.top_pos = degrees;
	futurePosition.front_left.top_pos = degrees;
	futurePosition.back_right.top_pos = degrees;
	futurePosition.front_right.top_pos = degrees;
	legChanges.top = 1;
}
void setLegAngle(leg_t * body, leg_pos_t * leg, uint8_t degreesTop, uint8_t degreesBottom){
	uint8_t channel = leg->pin;
	uint8_t skew = body->skew;

	if (degreesTop > SERVO_MAX)
		degreesTop = SERVO_MAX;
	else if (degreesTop < SERVO_MIN)
		degreesTop = SERVO_MIN;

	if (degreesBottom > SERVO_MAX)
		degreesBottom = SERVO_MAX;
	else if (degreesBottom < SERVO_MIN)
		degreesBottom = SERVO_MIN;


	if (channel == currentPosition->front_left.pin){
		if (skew < 0)
			degreesTop += skew;
		futurePosition.front_left.pin = channel;
		futurePosition.front_left.top_pos = degreesTop;
		futurePosition.front_left.bot_pos = degreesBottom;
		legChanges.legs |= 1UL << 3;
	} else if (channel == currentPosition->front_right.pin){
		if (skew > 0)
			degreesTop += skew;
		futurePosition.front_right.pin = channel;
		futurePosition.front_right.top_pos = degreesTop;
		futurePosition.front_right.bot_pos = degreesBottom;
		legChanges.legs |= 1UL << 2;
	} else if (channel == currentPosition->back_left.pin){
		if (skew < 0)
			degreesTop += skew;
		futurePosition.back_left.pin = channel;
		futurePosition.back_left.top_pos = degreesTop;
		futurePosition.back_left.bot_pos = degreesBottom;
		legChanges.legs |= 1UL << 1;
	} else if (channel == currentPosition->back_right.pin){
		if (skew > 0)
			degreesTop += skew;
		futurePosition.back_right.pin = channel;
		futurePosition.back_right.top_pos = degreesTop;
		futurePosition.back_right.bot_pos = degreesBottom;
		legChanges.legs |= 1UL << 0;
	}
}

leg_t getAllAngle(){

}

leg_pos_t getLegAngle(){

}

void updateServoAngles(){
	if (startLegStateMachineFlag == 1){
		//Check to see if all is changed
		if (legChanges.all == 1){
			setAllServoAngle(futurePosition.front_left.top_pos);
			legChanges.all = 0;
			return;
		}
		if (legChanges.top == 1){
			setTopServoAngle(futurePosition.front_left.top_pos);
			legChanges.top = 0;
		}

		if (legChanges.bottom == 1){
			setBottomServoAngle(futurePosition.front_left.bot_pos);
			legChanges.bottom = 0;
		}

		uint8_t number = legChanges.legs;
		if ((number >> 3) & 1U) {
			setLegServoAngle(currentPosition->front_left.pin,futurePosition.front_left.top_pos,futurePosition.front_left.bot_pos);
		}
		if ((number >> 2) & 1U) {
			setLegServoAngle(currentPosition->front_right.pin,futurePosition.front_right.top_pos,futurePosition.front_right.bot_pos);
		}
		if ((number >> 1) & 1U) {
			setLegServoAngle(currentPosition->back_left.pin,futurePosition.back_left.top_pos,futurePosition.back_left.bot_pos);
		}
		if ((number >> 0) & 1U) {
			setLegServoAngle(currentPosition->back_right.pin,futurePosition.back_right.top_pos,futurePosition.back_right.bot_pos);
		}
		legChanges.legs = 0;

		memcpy(currentPosition,&futurePosition,sizeof(legChanges_t));

		//Check to see if bottom is changed
		//Check to see if top is changed
		//Check to see if leg is changed
	}
}
