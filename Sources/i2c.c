/*
 * i2c.c
 *
 *  Created on: Oct 1, 2017
 *      Author: Andrew
 */
#include "S32K144.h"
#include "lpi2c1.h"
#include "i2c.h"
#include <stdio.h>





//file:///C:/NXP/S32DS_ARM_v2.0/S32DS/S32SDK_S32K14x_EAR_0.8.4/doc/html/group__lpi2c__drv.html
//https://www.nxp.com/docs/en/data-sheet/PCA9685.pdf
//https://learn.adafruit.com/16-channel-pwm-servo-driver/hooking-it-up

//Base address is 0x40

lpi2c_master_state_t lpi2c1MasterState;


void initI2C(void){
	/* Allocate memory for the LPI2C driver state structure */
	//
	status_t wtf;
	//LPI2C_SCFGR1_TXCFG(1);
	/* Declaration of the LPI2C transfer buffer */
	wtf = LPI2C_DRV_MasterInit(INST_LPI2C1, &lpi2c1_MasterConfig0, &lpi2c1MasterState);
}

uint8_t i2cRead8(uint8_t addr) {
	static uint8_t buffer;
	buffer = addr;
	//bengin transmission
	//write address
	//end transmission
	//get I2C data (callback)
	static uint8_t h;
	LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C1, &buffer, 1, true,OSIF_WAIT_FOREVER);
	LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C1, &h, 1, true,OSIF_WAIT_FOREVER);
	return h;
}

void i2cWrite8(uint8_t addr, uint8_t d) {
	static uint8_t buffer[2];
	buffer[0] = addr;
	buffer[1] = d;
	//begin transmission
	LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C1, buffer, 2, true,OSIF_WAIT_FOREVER);
	//end transmission
}

void i2cWriteBuffer8(uint8_t * buff, uint8_t length){
	LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C1, buff, length, true,OSIF_WAIT_FOREVER);
}



