/*
 * timers.c
 *
 *  Created on: Nov 26, 2017
 *      Author: Andrew Stroz
 */

#include "Cpu.h"
#include "timers.h"
#include "tasks.h"
#include "lpTmr1.h"
#include "stdio.h"

void lptmrISR(void);


void initTimer1(){
	LPTMR_DRV_Init(INST_LPTMR1, &lpTmr1_config0, false);
	/* Install IRQ handler for LPTMR interrupt */
	INT_SYS_InstallHandler(LPTMR0_IRQn, &lptmrISR, (isr_t *)0);
	/* Enable IRQ for LPTMR */
	INT_SYS_EnableIRQ(LPTMR0_IRQn);
	/* Start LPTMR counter */
	LPTMR_DRV_StartCounter(INST_LPTMR1);
}

void lptmrISR(void)
{
	/* Clear compare flag */
	LPTMR_DRV_ClearCompareFlag(INST_LPTMR1);
	/* Toggle Timer State Machine */
	//printf("Hello Andrei :) \r\n");

	tick = 1;

}
