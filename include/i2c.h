/*
 * i2c.h
 *
 *  Created on: Oct 1, 2017
 *      Author: Andrew
 */

void initI2C(void);
void i2cWrite8(uint8_t addr, uint8_t d);
uint8_t i2cRead8(uint8_t addr);
void i2cWriteBuffer8(uint8_t * buff, uint8_t length);


