/*
 * cfg.h
 *
 *  Created on: Jan 14, 2018
 *      Author: andyd
 */

#ifndef CFG_H_
#define CFG_H_
#include "Cpu.h"

void initCfg(void);

#include "Cpu.h"

typedef enum {FORWARD, LEFT, RIGHT, BACK} dir_t;
typedef enum {ALG1, ALG2} style_t;

typedef struct leg_pos_t{
	uint8_t pin;
	uint8_t top_pos, bot_pos;
} leg_pos_t;

typedef struct leg_t{
	leg_pos_t front_left, front_right, back_left, back_right;
	uint8_t skew;
	dir_t dir;
	style_t style;
} leg_t;


// experimental angular skew to movement
#define DIR_SKEW_DEFAULT		0

#define BOT_LEG_DEFAULT			90
#define BOT_LEG_UP 				(BOT_LEG_DEFAULT + 45)
#define BOT_LEG_DOWN			(BOT_LEG_DEFAULT - 45)

#define TOP_LEG_DEFAULT			90
#define TOP_LEG_FORWARD			TOP_LEG_DEFAULT + 45
#define TOP_LEG_BACKWARD		TOP_LEG_DEFAULT - 45

#define SERVO_MIN				0
#define SERVO_MAX				180

// different directions, could have made the struct an array and done memmove() to shift array appropriately
#define DIR_LEG_DEFAULT 		(leg_t){.front_left=(leg_pos_t){.pin=2, .top_pos=TOP_LEG_DEFAULT, .bot_pos=BOT_LEG_DEFAULT},\
										.front_right=(leg_pos_t){.pin=4, .top_pos=TOP_LEG_DEFAULT, .bot_pos=BOT_LEG_DEFAULT},\
										.back_left=(leg_pos_t){.pin=0, .top_pos=TOP_LEG_DEFAULT, .bot_pos=BOT_LEG_DEFAULT},\
										.back_right=(leg_pos_t){.pin=6, .top_pos=TOP_LEG_DEFAULT, .bot_pos=BOT_LEG_DEFAULT},\
										.skew=DIR_SKEW_DEFAULT,\
										.style=ALG2}

#define DIR_LEG_FORWARD 		(leg_t){.front_left=DIR_LEG_DEFAULT.front_left,\
										.front_right=DIR_LEG_DEFAULT.front_right,\
										.back_left=DIR_LEG_DEFAULT.back_left,\
										.back_right=DIR_LEG_DEFAULT.back_right}

#define DIR_LEG_RIGHT 			(leg_t){.front_left=DIR_LEG_DEFAULT.back_left,\
										.front_right=DIR_LEG_DEFAULT.front_left,\
										.back_left=DIR_LEG_DEFAULT.back_right,\
										.back_right=DIR_LEG_DEFAULT.front_right}

#define DIR_LEG_LEFT 			(leg_t){.front_left=DIR_LEG_DEFAULT.front_right,\
										.front_right=DIR_LEG_DEFAULT.back_right,\
										.back_left=DIR_LEG_DEFAULT.front_left,\
										.back_right=DIR_LEG_DEFAULT.back_left}

#define DIR_LEG_BACKWARD		(leg_t){.front_left=DIR_LEG_DEFAULT.back_right,\
										.front_right=DIR_LEG_DEFAULT.back_left,\
										.back_left=DIR_LEG_DEFAULT.front_right,\
										.back_right=DIR_LEG_DEFAULT.front_left}

#endif /* CFG_H_ */
