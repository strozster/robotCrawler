/*
 * tasks.h
 *
 *  Created on: Oct 23, 2017
 *      Author: Andrew Stroz
 */

#ifndef _TASKS_H_
#define	_TASKS_H_

#include "Cpu.h"

extern uint8_t tick;

#define TICKS_PER_MILLISECOND   1
#define TASK_1MS                TICKS_PER_MILLISECOND
#define TASK_5MS                (TASK_1MS * 5)
#define TASK_10MS               (TASK_1MS * 10)
#define TASK_25MS               (TASK_1MS * 25)
#define TASK_50MS               (TASK_1MS * 50)
#define TASK_100MS              (TASK_1MS * 100)
#define TASK_250MS              (TASK_1MS * 250)
#define TASK_500MS              (TASK_1MS * 500)
#define TASK_1000MS             (TASK_1MS * 1000)
#define TASK_2000MS             (TASK_1MS * 2000)

typedef struct {

    const uint16_t resetCount;
    uint16_t counter;
    void(*taskFunction)();

} Tasks;


typedef struct {
    uint16_t counter;
    uint16_t duration;
    bool isComplete;
} StartUp;

void TaskScheduler();


#endif

