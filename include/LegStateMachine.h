/*
 * LegStateMachine.h
 *
 *  Created on: Jan 28, 2018
 *      Author: Andrew Stroz
 */

#ifndef LEGSTATEMACHINE_H_
#define LEGSTATEMACHINE_H_

#include "cfg.h"

typedef struct legChanges_t {
	uint8_t all;
	uint8_t top;
	uint8_t bottom;
	uint8_t legs;
} legChanges_t ;

void setAllAngle(uint8_t degrees);
void setBottomAngle(uint8_t degrees);
void setTopAngle(uint8_t degrees);
void setLegAngle(leg_t * body, leg_pos_t * leg, uint8_t degreesTop, uint8_t degreesBottom);

void startLegStateMachine(leg_t * currentState);
leg_t getAllAngle();
leg_pos_t getLegAngle();
void updateServoAngles();


#endif /* LEGSTATEMACHINE_H_ */
