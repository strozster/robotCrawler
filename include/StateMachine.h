/*
 * StateMachine.h
 *
 *  Created on: Oct 23, 2017
 *      Author: Andrew Stroz
 */

#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_
#include "Cpu.h"
#include "cfg.h"

#define STATE_START_DELAY	1000

typedef void(*STATE_FPTR)(bool);

void StateMachineInit();
void StateMachineTask(uint16_t taskPeriod);

typedef enum{WALK, ROTATE} sequencer_t;

#endif  // _STATE_MACHINE_H_


