/*
 * pca9685.h
 *
 *  Created on: Oct 22, 2017
 *      Author: Andrew Stroz
 */

#ifndef PCA9685_H_
#define PCA9685_H_

#include "Cpu.h"

#define PCA9685_MODE1 					0x0
#define PCA9685_PRESCALE 				0xFE
#define LED0_ON_L 						0x6
#define LED0_ON_H 						0x7

#define SERVO_ROTATION_LENGTH 			180 // degrees
#define SERVO_TICKS_LENGTH 				4096 // ticks

#define SERVO_POSITION_0 				2.5 // ms
#define SERVO_POSITION_90 				1.5 // ms
#define SERVO_POSITION_180 				0.5 // ms

void sendInitaliztionDataToPCA9685(void);
void setPWMFreq(float freq);
void setPWMDuty(uint8_t channel, uint8_t degrees);
void setAllServoAngle(uint8_t degrees);
void setBottomServoAngle(uint8_t degrees);
void setTopServoAngle(uint8_t degrees);
void setLegServoAngle(uint8_t channel, uint8_t degreesTop, uint8_t degreesBottom);

float servo_tick_rate;
float servo_tick_bias;
float servo_tick_angular_res;


#endif /* PCA9685_H_ */
